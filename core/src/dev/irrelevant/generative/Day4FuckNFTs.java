package dev.irrelevant.generative;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ScreenUtils;

import java.util.Random;

public class Day4FuckNFTs implements IGenuaryEntry {

    private final Random rand = new Random();

    class Letter {
        Line[] lines;
    }
    class F extends Letter {
        public F(Line v, Line t, Line m) {
            vBar = v;
            tBar = t;
            mBar = m;
            lines = new Line[] { vBar, tBar, mBar };
        }
        Line vBar;
        Line tBar;
        Line mBar;
    }
    class U extends Letter {
        Line lBar;
        Line bBar;
        Line rBar;
        public U(Line l, Line b, Line r) {
            lBar = l; bBar = b; rBar = r;
            lines = new Line[] { l, b, r };
        }
    }
    class C extends Letter {
        public C(Line t, Line l, Line b) {
            lines = new Line[] { t, l, b };
        }
    }

    class K extends Letter {
        public K(Line v, Line u, Line d) {
            lines = new Line[] { v, u, d };
        }
    }

    class N extends Letter {
        public N(Line l, Line m, Line r) {
            lines = new Line[] { l, m, r };
        }
    }

    class T extends Letter {
        public T(Line t, Line v) {
            lines = new Line[] { t, v };
        }
    }
    class S extends Letter {
        public S(Line t, Line tl, Line m, Line br, Line b) {
            lines = new Line[] { t, tl, m, br, b };
        }
    }

    private final int count = 100;
    private final Letter[] fs;
    private final Letter[] us;
    private final Letter[] cs;
    private final Letter[] ks;
    private final Letter[] ns;
    private final Letter[] fs_;
    private final Letter[] ts;
    private final Letter[] ss;
    private final Color[] colors;

    public Day4FuckNFTs() {
        // Generate Letters

        fs = new F[count];
        us = new U[count];
        cs = new C[count];
        ks = new K[count];
        ns = new N[count];
        fs_ = new F[count];
        ts = new T[count];
        ss = new S[count];
        colors = new Color[count];
        generateLetters();
    }

    private void generateLetters() {
        float boxW = Gdx.graphics.getWidth() / 2f;
        float boxH = Gdx.graphics.getHeight() / 2f;
        float l = 5f;
        float t = 5f;
        for (int i = 0; i < count; i++) {
            float left = (rand.nextFloat() * (boxW * 2f));
            float top = (rand.nextFloat() * (boxW * 2f));
            float h = (rand.nextFloat() * (boxH / 2f));
            float w = (rand.nextFloat() * (boxW / 4f));
//            float boxModifier = i == 0 ? 1f : i * 2f;
//            float modW = boxW / boxModifier;
//            float modH = boxH / boxModifier;
//            l = l + i * modW;
//            t = t + i * modH;
//            float maxH = modH / 2f;
//            float maxW = modW / 4f;
//            float h = maxH * (count - i) / count;
//            float w = maxW * (count - i) / count;
//            float left = l; //5f + (i / count) * (maxW * 4);
//            float top = t; //5f + (i / count) * (maxH * 2);
            fs[i] = buildF(h, w, left, top);
            us[i] = buildU(h, w, left + w, top);
            cs[i] = buildC(h, w, left + (w*2), top);
            ks[i] = buildK(h, w, left + (w*3), top);

            ns[i] = buildN(h, w, left, top + h);
            fs_[i] = buildF(h, w, left + w, top + h);
            ts[i] = buildT(h, w, left + (w*2), top + h);
            ss[i] = buildS(h, w, left + (w*3), top + h);

            colors[i] = new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), 1f);
        }
    }

    @Override
    public void render(float deltaTime, ShapeRenderer shaper) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            generateLetters();
        }
        ScreenUtils.clear(1f, 1f, 1f, 1f);
        shaper.begin(ShapeRenderer.ShapeType.Filled);
        shaper.setColor(1f, 1f, 1f, 1f);

//        drawF(shaper, 50f, 50f, 200f, 75f);
        for (int i = 0; i < count; i++) {
            shaper.setColor(colors[i]);
            Letter f = fs[i];
            if (f != null) { drawLetter(f, shaper); }
            Letter u = us[i];
            if (u != null) { drawLetter(u, shaper); }
            Letter c = cs[i];
            if (c != null) { drawLetter(c, shaper); }
            Letter k = ks[i];
            if (k != null) { drawLetter(k, shaper); }
            Letter n = ns[i];
            if (n != null) { drawLetter(n, shaper); }
            Letter f_ = fs_[i];
            if (f_ != null) { drawLetter(f_, shaper); }
            Letter t = ts[i];
            if (t != null) { drawLetter(t, shaper); }
            Letter s = ss[i];
            if (s != null) { drawLetter(s, shaper); }
        }

        shaper.end();
    }

    private void drawLetter(Letter letter, ShapeRenderer shaper) {
        for (int l = 0; l < letter.lines.length; l++) {
            Line line = letter.lines[l];
            shaper.rectLine(line.start, line.end, line.width);
        }
    }

    private F buildF(float height, float width, float left, float top) {
        float vX = rand.nextFloat() * (width * 0.2f) + left;
        Line vBar = new Line();
        vBar.width = rand.nextFloat() * (width * 0.08f) + 2f;
        vBar.start = new Vector2(vX, top);
        vBar.end = new Vector2(vX, top + height);

        float topOffset = rand.nextFloat() * (height * 0.2f);
        float tY = topOffset + top;
        Line tBar = new Line();
        tBar.width = rand.nextFloat() * (height * 0.08f) + 2f;
        tBar.start = new Vector2(left, tY);
        tBar.end = new Vector2(left + width, tY);

        float mY = rand.nextFloat() * (height * 0.2f) + tY + topOffset;
        Line mBar = new Line();
        mBar.width = rand.nextFloat() * (height * 0.08f) + 2f;
        mBar.start = new Vector2(left, mY);
        mBar.end = new Vector2(left + width, mY);

        return new F(vBar, tBar, mBar);
    }

    private U buildU(float height, float width, float left, float top) {
        float lX = rand.nextFloat() * (width * 0.1f) + left;
        Line lBar = new Line();
        lBar.start = new Vector2(lX, top);
        lBar.end = new Vector2(lX, top + height);
        lBar.width = rand.nextFloat() * (width * 0.05f) + 2f;

        float bY = (top + height) - rand.nextFloat() * (height * 0.1f);
        Line bBar = new Line();
        bBar.start = new Vector2(left, bY);
        bBar.end = new Vector2(left + width, bY);
        bBar.width = rand.nextFloat() * (height * 0.08f) * 2f;

        float rX = (left + width) - rand.nextFloat() * (width * 0.1f);
        Line rBar = new Line();
        rBar.start = new Vector2(rX, top);
        rBar.end = new Vector2(rX, top + height);
        rBar.width = rand.nextFloat() * (width * 0.05f) + 2f;

        return new U(lBar, bBar, rBar);
    }

    private C buildC(float height, float width, float left, float top) {
        float tY = top + rand.nextFloat() * (width * 0.1f);
        float hWidth = rand.nextFloat() * (height * 0.05f) + 2f;
        Line tBar = new Line();
        tBar.start = new Vector2(left, tY);
        tBar.end = new Vector2(left + width, tY);
        tBar.width = hWidth;

        float lX = rand.nextFloat() * (width * 0.1f) + left;
        Line lBar = new Line();
        lBar.start = new Vector2(lX, top);
        lBar.end = new Vector2(lX, top + height);
        lBar.width = rand.nextFloat() * (width * 0.05f) + 2f;

        float bY = (top + height) - rand.nextFloat() * (height * 0.1f);
        Line bBar = new Line();
        bBar.start = new Vector2(left, bY);
        bBar.end = new Vector2(left + width, bY);
        bBar.width = hWidth;

        return new C(tBar, lBar, bBar);
    }

    private K buildK(float height, float width, float left, float top) {
        float lX = rand.nextFloat() * (width * 0.1f) + left;
        Line lBar = new Line();
        lBar.start = new Vector2(lX, top);
        lBar.end = new Vector2(lX, top + height);
        lBar.width = rand.nextFloat() * (width * 0.05f) + 2f;

        float mergeX = lX;
        float mergeY = top + (height / 2f) + (rand.nextFloat() * height * 0.03f);
        float hWidth = rand.nextFloat() * (height * 0.05f) + 2f;

        Line tBar = new Line();
        tBar.start = new Vector2(mergeX, mergeY);
        tBar.end = new Vector2(left + width, top);
        tBar.width = hWidth;

        Line bBar = new Line();
        bBar.start = new Vector2(mergeX, mergeY);
        bBar.end = new Vector2(left + width, top + height);
        bBar.width = hWidth;

        return new K(lBar, tBar, bBar);
    }

    private N buildN(float height, float width, float left, float top) {
        float lX = rand.nextFloat() * (width * 0.1f) + left;
        float vWidth = rand.nextFloat() * (width * 0.05f) + 2f;
        Line lBar = new Line();
        lBar.start = new Vector2(lX, top);
        lBar.end = new Vector2(lX, top + height);
        lBar.width = vWidth;

        float rX = left + width - rand.nextFloat() * (width * 0.1f);
        Line rBar = new Line();
        rBar.start = new Vector2(rX, top);
        rBar.end = new Vector2(rX, top + height);
        rBar.width = vWidth;

        float mergeOffset = rand.nextFloat() * (height * 0.02f);
        float hWidth = rand.nextFloat() * (height * 0.05f) + 2f;

        Line mBar = new Line();
        mBar.start = new Vector2(lX, top + mergeOffset);
        mBar.end = new Vector2(rX, top + height - mergeOffset);
        mBar.width = hWidth;

        return new N(lBar, mBar, rBar);
    }

    private T buildT(float height, float width, float left, float top) {
        float tY = rand.nextFloat() * (height * 0.1f) + top;
        Line tBar = new Line();
        tBar.start = new Vector2(left, tY);
        tBar.end = new Vector2(left + width, tY);
        tBar.width = rand.nextFloat() * height * 0.1f + 2f;

        float vX = left + width * 0.5f + (rand.nextFloat() - 0.5f) * (width * 0.1f);
        Line vBar = new Line();
        vBar.start = new Vector2(vX, top);
        vBar.end = new Vector2(vX, top + height);
        vBar.width = rand.nextFloat() * width * 0.1f + 2f;

        return new T(tBar, vBar);
    }

    private S buildS(float height, float width, float left, float top) {
        float vOffset = rand.nextFloat() * (height * 0.1f);
        float hWidth = rand.nextFloat() * height * 0.1f + 2f;
        float tY = vOffset + top;
        Line tBar = new Line();
        tBar.start = new Vector2(left, tY);
        tBar.end = new Vector2(left + width, tY);
        tBar.width = hWidth;

        float vWidth = rand.nextFloat() * width * 0.1f + 2f;
        float vX = left + width * 0.5f + (rand.nextFloat() - 0.5f) * (width * 0.1f);
        float midY = top + (height * 0.5f) + (rand.nextFloat() - 0.5f) * (height * 0.1f);
        Line tlBar = new Line();
        tlBar.start = new Vector2(left, tY);
        tlBar.end = new Vector2(left, midY);
        tlBar.width = vWidth;


        Line mBar = new Line();
        mBar.start = new Vector2(left, midY);
        mBar.end = new Vector2(left + width, midY);
        mBar.width = hWidth;

        float bY = top + height - vOffset;

        Line brBar = new Line();
        brBar.start = new Vector2(left + width, midY);
        brBar.end = new Vector2(left + width, bY);
        brBar.width = vWidth;

        Line bBar = new Line();
        bBar.start = new Vector2(left, bY);
        bBar.end = new Vector2(left + width, bY);
        bBar.width = hWidth;

        return new S(tBar, tlBar, mBar, brBar, bBar);
    }
    private void drawF(ShapeRenderer shaper, float x, float y, float height, float width) {
        float left = x - (width / 2f);
        float top = y - (height / 2f);

        Line vBar = getLine(height, width, left, top, true);
        Line topHBar = getLine(height, width, left, top, false);
        Line bottmHBar = getLine(height, width, left, top, false);

        shaper.rectLine(vBar.start, vBar.end, vBar.width);
        shaper.rectLine(topHBar.start, topHBar.end, topHBar.width);
        shaper.rectLine(bottmHBar.start, bottmHBar.end, bottmHBar.width);
    }

    private Line getLine(float height, float width, float left, float top, boolean isVertical) {
        float minLen = isVertical ? height / 2f : width / 2f;
        float maxLen = isVertical ? height : width;
        float minWidth = isVertical ? width / 5f : height / 10f;
        float maxWidth = isVertical ? width / 2f : height / 4f;
        Vector2 lineSize = rLineSize(minLen, maxLen, minWidth, maxWidth);

        float maxStartX = isVertical ? (width / 2f) : (width - lineSize.x);
        float maxStartY = isVertical ? (height - lineSize.x) : (height / 2f);
        Vector2 lineStart = new Vector2(
        rand.nextFloat() * maxStartX + left,
        rand.nextFloat() * maxStartY + top
        );
        float maxEndX = isVertical ? lineStart.x : lineStart.x + lineSize.x; //rand.nextFloat() * (width / 2f) : lineStart.x + lineSize.x;
        float maxEndY = isVertical ? lineStart.y + lineSize.y : lineStart.y; //lineStart.y + lineSize.x : rand.nextFloat() * (height / 2f);
        Vector2 lineEnd = new Vector2(maxEndX, maxEndY);

        Line vBar = new Line();
        vBar.start = lineStart;
        vBar.end = lineEnd;
        vBar.width = lineSize.y;
        return vBar;
    }

    private Vector2 rLineSize(float minLen, float maxLen, float minWidth, float maxWidth) {
        return new Vector2(
            (maxLen - minLen) * MathUtils.clamp(rand.nextFloat() + 0.5f, 0.5f, 1f),
            (maxWidth - minWidth) * MathUtils.clamp(rand.nextFloat() + 0.15f, 0.15f, 1f)
        );
    }
}
