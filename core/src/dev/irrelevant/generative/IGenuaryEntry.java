package dev.irrelevant.generative;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public interface IGenuaryEntry {

    public void render(float deltaTime, ShapeRenderer shaper);
}
