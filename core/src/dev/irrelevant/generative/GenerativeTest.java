package dev.irrelevant.generative;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.ScreenUtils;

import java.util.Random;

public class GenerativeTest extends ApplicationAdapter {
	ShapeRenderer shaper;
	OrthographicCamera cam;
	Day1TenThousandDots points;
	Day4FuckNFTs fuckNFTs;
	CheckerBoard checkerBoard;
	PerlinMap perlinMap;

	IGenuaryEntry currentEntry;

	@Override
	public void create () {
		points = new Day1TenThousandDots(100);
		fuckNFTs = new Day4FuckNFTs();
		checkerBoard = new CheckerBoard();
		perlinMap = new PerlinMap();

		//currentEntry = points;
		//currentEntry = fuckNFTs;
		//currentEntry = checkerBoard;
		currentEntry = perlinMap;
		checkerBoard.updateSize();
		cam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		shaper = new ShapeRenderer();
		shaper.setProjectionMatrix(cam.combined);
	}

	@Override
	public void render () {
		cam.update();
		currentEntry.render(Gdx.graphics.getDeltaTime(), shaper);
	}

	@Override
	public void dispose () {
		shaper.dispose();
		cam = null;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		checkerBoard.updateSize();
	}
}
