package dev.irrelevant.generative;

import com.badlogic.gdx.math.Vector2;

public class Line {
    float len;
    float width;
    Vector2 start;
    Vector2 end;
}
