package dev.irrelevant.generative;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.ScreenUtils;

public class PerlinMap implements IGenuaryEntry{
    private float[][] points;
    private Color[][] colors;

    private Color[] startingColors = new Color[] {
            Color.RED,
            Color.BLUE,
            Color.GREEN,
            Color.CYAN,
            Color.MAGENTA,
            Color.ORANGE,
            Color.YELLOW
    };

    private Color[] endingColors = new Color[] {
            Color.YELLOW,
            Color.GREEN,
            Color.RED,
            Color.PURPLE,
            Color.CYAN,
            Color.BLUE,
            Color.ORANGE
    };

    private float elapsedTime;
    private int octaves = 3;
    private float boxSize = 5f;
    private int colorIndex = 0;
    public PerlinMap() {
        generatePoints();
    }

    public PerlinMap(float blockSize) {
        boxSize = blockSize;
    }

    @Override
    public void render(float deltaTime, ShapeRenderer shaper) {
        elapsedTime += deltaTime;

        if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
            colorIndex = (colorIndex + 1) % startingColors.length;
            calculateColors();
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
            if (colorIndex > 0) {
                colorIndex = (colorIndex - 1) % startingColors.length;
            } else {
                colorIndex = startingColors.length - 1;
            }
            calculateColors();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            boxSize += 1f;
            generatePoints();
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN) && boxSize > 1f) {
            boxSize -= 1f;
            generatePoints();
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            generatePoints();
        }

        ScreenUtils.clear(0, 0, 0, 1);
        shaper.begin(ShapeRenderer.ShapeType.Filled);
        shaper.setColor(1f, 1f, 1f, 1f);

//        float colorMod = MathUtils.sinDeg(elapsedTime);
//        float r = elapsedTime

        float w = points.length;
        float h = points[0].length;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                float intensity = points[i][j];
                Color color = colors[i][j];
                shaper.setColor(color); //intensity, intensity, intensity, 1f);
                float x = i * boxSize;
                float y = j * boxSize;
                shaper.rect(x, y, boxSize, boxSize);
            }
        }
        shaper.end();
    }

    private void calculateColors() {
        colors = MapGradient(startingColors[colorIndex], endingColors[colorIndex], points);
    }
    private void generatePoints() {
        points = PerlinNoiseGenerator.generatePerlinNoise(
                MathUtils.ceil(Gdx.graphics.getWidth() / boxSize),
                MathUtils.ceil(Gdx.graphics.getHeight()/boxSize),
                octaves);
        calculateColors();
    }

    Color GetColor(Color gradientStart, Color gradientEnd, float t)
    {
        float u = 1 - t;

        return new Color(
                gradientStart.r * u + gradientEnd.r * t,
                gradientStart.g * u + gradientEnd.g * t,
                gradientStart.b * u + gradientEnd.b * t,
                1f);
    }

    Color[][] MapGradient(Color gradientStart, Color gradientEnd, float[][] perlinNoise)
    {
        int width = perlinNoise.length;
        int height = perlinNoise[0].length;

        Color[][] image = new Color[width][height];

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                image[i][j] = GetColor(gradientStart, gradientEnd, perlinNoise[i][j]);
            }
        }

        return image;
    }
}
