package dev.irrelevant.generative;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.ScreenUtils;

public class CheckerBoard implements IGenuaryEntry{

    float windowWidth;
    float windowHeight;
    private int unitSize = 2;
    private float elapsedTime = 0f;

    public CheckerBoard() {
        updateSize();
    }

    public void updateSize() {
        windowWidth = Gdx.graphics.getWidth();
        windowHeight = Gdx.graphics.getHeight();
    }

    @Override
    public void render(float deltaTime, ShapeRenderer shaper) {
        elapsedTime += deltaTime;
        unitSize = MathUtils.ceil(elapsedTime);
//        unitSize += 1;
        ScreenUtils.clear(1f, 0, 0, 1);
        shaper.begin(ShapeRenderer.ShapeType.Filled);
        shaper.setColor(1f, 0f, 0f, 1f);
        for (int x = 0; x < windowWidth; x++) {
            for (int y = 0; y < windowHeight; y++) {
                float intensity = getCheckerboardIntensityForPoint(
                        x / windowWidth,
                        y / windowHeight
                );
                shaper.setColor(
                        intensity * (x / 100f),
                        intensity * (y / 100f),
                        intensity * ((x+y) % 100f / 100f),
                        1f
                );
//                shaper.setColor(intensity, intensity, intensity, 1f);
                shaper.rect(x, y, 1f, 1f);
            }
        }
        shaper.end();
    }

    private float getCheckerboardIntensityForPoint(float u, float v) {
        return (MathUtils.floor(unitSize*u) + MathUtils.floor(unitSize*v)) % 2;
    }
}
