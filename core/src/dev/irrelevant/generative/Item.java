package dev.irrelevant.generative;

public class Item {
    private float originalX;
    private float originalY;
    private float originalSize;
    float x;
    float y;
    float size;
    public Item(float x, float y, float size) {
        this.x = x;
        this.y = y;
        this.size = size;
        originalX = x;
        originalY = y;
        originalSize = size;
    }


    public float getOriginalSize() {
        return originalSize;
    }

    public float getOriginalY() {
        return originalY;
    }

    public float getOriginalX() {
        return originalX;
    }
}
