package dev.irrelevant.generative;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.ScreenUtils;

import java.util.Random;

public class Day1TenThousandDots implements IGenuaryEntry {

    private static final float full_size = 6f;
    private static final float half_size = full_size / 2f;
    private static final Random rand = new Random();

    private final Item[] points;
    private final int pointCount;
    private final int sideSize;
    private float elapsedTime;

    public Day1TenThousandDots(int sizeRoot) {
        int fullSize = sizeRoot * sizeRoot;
        sideSize = sizeRoot;
        pointCount = fullSize;
        points = new Item[fullSize];
        for(int i = 0; i < sizeRoot; i++) {
            for (int j = 0; j < sizeRoot; j++) {
                Item point = createPointForGridPosition(i, j);
                int pos = i * sideSize + j;
                points[pos] = point;
            }
        }
        generatePoints(rand.nextFloat()* rand.nextInt(), false);
    }

    @Override
    public void render(float deltaTime, ShapeRenderer shaper) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            generatePoints(Gdx.graphics.getDeltaTime(), false);
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.C)) {
            generatePoints(Gdx.graphics.getDeltaTime(), true);
        }
        ScreenUtils.clear(0, 0, 0, 1);
        shaper.begin(ShapeRenderer.ShapeType.Filled);
        shaper.setColor(1f, 1f, 1f, 1f);

        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                Item point = getPointForGridPosition(i, j);
                shaper.setColor(
                        i / 100f,
                        j / 100f,
                        (i+j) % 100f / 100f,
                        1f
                );
                switch(j % 10) {
                    case 0:
                        shaper.setColor(1f, 1f, 1f, 1f);
                        break;
                    case 1:
                }
                float x = point.x % Gdx.graphics.getWidth();
                float y = point.y % Gdx.graphics.getHeight();
                float size = point.size;
                shaper.circle(x, y, size, 10);
            }
        }
        shaper.end();
    }

    private void generatePoints(float deltaTime, boolean isChaotic) {
        elapsedTime += deltaTime;

        float modifier = MathUtils.sinDeg(elapsedTime % 10);
        float xRand = 1f, yRand = 1f, sRand = 1f;

        if (!isChaotic) {
            xRand = rand.nextFloat() * rand.nextInt(20);
            yRand = rand.nextFloat() * rand.nextInt(20);
            sRand = rand.nextFloat() * rand.nextInt(20);
        }

        for(int i = 0; i < pointCount; i++) {
            if (isChaotic) {
                xRand = rand.nextInt(20);
                yRand = rand.nextInt(20);
                sRand = rand.nextInt(20);
            }
            Item point = points[i];
            point.x = point.getOriginalX() * (modifier + xRand) + point.getOriginalX();
            point.y = point.getOriginalY() * (modifier + yRand) + point.getOriginalY();
            point.size = point.getOriginalSize() * (sRand * modifier) + point.getOriginalSize();
        }
    }

    private Item getPointForGridPosition(int x, int y) {
        int pos = x * sideSize + y;
        return points[pos];
    }

    private Item createPointForGridPosition(int x, int y) {
        return new Item(
            half_size + x * full_size,
            half_size + y * full_size,
                half_size
        );
    }
}
